# Longest Words

Write a function that returns the longest word(s) from a sentence. The function should not return any duplicate words (case-insensitive).

## Example

```js
f("You are just an old antidisestablishmentarian") // ["antidisestablishmentarian"]
f("I gave a present to my parents") // ["present", "parents"]
f("Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo") // ["buffalo"] or ["Buffalo"]
```
