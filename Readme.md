# Code Problems

This is my repo full of code problems that I have completed prior to or during an interview. I hope that all these problems (and solutions) are useful to practice against and review. Feel free to contribute any solutions and optimisations, given they are algorithmic optimisations. Please add your own problems that you find as well, as I would love to see them.

## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality.

Please lint and test your code with any means available - currently only JavaScript has tests and linting via Mocha and JSHint.

## Testing

```sh
npm install # Installs `mocha` and any other dependencies needed to run
npm test    # Runs the testing scripts
```
